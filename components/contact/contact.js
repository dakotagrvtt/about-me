/* global localStorage */
console.log('SCRIPT START')

//loads old local storage data
window.addEventListener('load', (event) => {
    console.log('Starting initialization ')
    if (localStorage.getItem('name')) {
        document.querySelector('#name').value = localStorage.getItem('name')
        console.log(`Stored name = ${localStorage.name}`)
    }
    if (localStorage.getItem('email')) {
        document.querySelector('#email').value = localStorage.getItem('email')
        console.log(`Stored email = ${localStorage.email}`)
    }
    if (localStorage.getItem('comment')) {
        document.querySelector('#comment').value = localStorage.getItem('comment')
        console.log(`Stored comment = ${localStorage.comment}`)
    }
    console.log('Finished initialization')
})

document.querySelector('#submit').addEventListener('click', () => {
    const regexNames = /[^a-z A-Z'_]/g
    const regexMsg = /[^a-z A-Z0-9!@#$%^&*/({})_+-=:";'?,.\n]/g
    const name = document.querySelector('#name').value.replace(regexNames, '')
    const email = document.querySelector('#email').value
    const comment = document.querySelector('#comment').value.replace(regexMsg, '')

    const result = `<br><h3>Your message was sent!</h3><p><b>Name: </b>${name}</p><p><b>Email: </b>${email}</p><p>${comment}</p>`

    document.querySelector('#display-message').innerHTML = result
})

document.querySelector('#save').addEventListener('click', () => {
    console.log('Start save')
    const regexNames = /[^a-z A-Z'_]/g
    const regexMsg = /[^a-z A-Z0-9!@#$%^&*/({})_+-=:";'?,.\n]/g
    const name = document.querySelector('#name').value.replace(regexNames, '')
    const email = document.querySelector('#email').value
    const comment = document.querySelector('#comment').value.replace(regexMsg, '')

    localStorage.setItem('name', name)
    localStorage.setItem('email', email)
    localStorage.setItem('comment', comment)
    console.log('Set items to local storage')
})

document.querySelector('#clear').addEventListener('click', () => {
    console.log('Starting local storage clear handler')
    localStorage.removeItem('name')
    localStorage.removeItem('email')
    localStorage.removeItem('comment')
    console.log('Finished clearing - localStorage entries removed')
  })