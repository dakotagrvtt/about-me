console.log('SCRIPT START SUCCESS')

const testInput = (seconds) => { return seconds; }

const calcMin = (seconds) => { return seconds / 60; }

const calcHrs = (seconds) => { return seconds / 60 / 60; }

const calcDays = (seconds) => { return seconds / 60 / 60 / 24; }

const calcYrs = (seconds) => { return seconds / 60 / 60 / 24 / 365.2425; }
/*https://www.rapidtables.com/calc/time/days-in-year.html
  for accurate average days in a year based on the Gregorian calendar
*/

const convert = new Vue({
    el: '#seconds-converter',
    data: {
        seconds: 0
    },
    computed: {
        result: function () {
            const sec = parseFloat(this.seconds)

            const test = testInput(sec)
            if (isNaN(test)) {
                return `Enter any number of seconds`
            }
            else {
                if (sec < 0){
                    return `Must be positive!`
                }
                else if (sec >= 31536000) {
                    return `${calcYrs(sec).toFixed(3)} year(s)!`
                }
                else if (sec >= 86400) {
                    return `${calcDays(sec).toFixed(3)} day(s)`
                }
                else if (sec >= 3600) {
                    return `${calcHrs(sec).toFixed(3)} hour(s)`
                }
                else if (sec >= 60) {
                    return `${calcMin(sec).toFixed(3)} minute(s)`
                }
                else {
                    return `${sec} second(s)`
                }
            }
        }
    }
})