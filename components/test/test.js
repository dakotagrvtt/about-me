QUnit.module('MAIN MODULE', {})  // group all these tests together

QUnit.test('Test functions', assert => {
  assert.equal(calcMin(61).toFixed(2), 1.02, 'Minutes check')
  assert.equal(calcHrs(3801).toFixed(2), 1.06, 'Hours check')
  assert.equal(calcDays(87401).toFixed(2), 1.01, 'Days check')
  assert.equal(calcYrs(41536001).toFixed(2), 1.32, 'Years check')
})

// QUnit.config.autostart = false  // sync = false; start after loading html

// window.addEventListener('load', () => {
//   const appURL = '../time-converter.html'
//   const openingTag = '<main>'
//   const closingTag = '</main>'
//   fetch(appURL, { method: 'GET' })
//     .then(response => {
//       return response.text() // returns promise
//     })
//     .then(txt => {
//       const start = txt.indexOf(openingTag)
//       const end = txt.indexOf(closingTag) + closingTag.length
//       const html = txt.substring(start, end)
//       const qunitFixtureBody = document.getElementById('qunit-fixture')
//       qunitFixtureBody.innerHTML = html
//       console.info(qunitFixtureBody)
//       QUnit.start()
//     })
//     .catch(error => {
//       console.error('error:', error);
//       QUnit.start()
//     })
// })

// QUnit.test("TEST first number validation", assert => {
//   const input = document.querySelector('#firstNumber')
//   const warning = document.querySelector('#firstWarning')
//   input.value = -3;
//   assert.equal(input.value, -3, "Bad value assigned")
//   assert.strictEqual(input.checkValidity(), false, "Correctly fails validation")
//   input.focus()
//   input.blur()
//   assert.strictEqual(warning.innerHTML, 'Invalid input', `Correctly adds warning ${warning}`)
// })