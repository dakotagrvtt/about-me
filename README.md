# About Me

* [**Source Code**](https://github.com/dakotagrvtt/about-me)
* [**GitHub Page**](https://dakotagrvtt.github.io/about-me/)

## **About**
This page will be about how to setup a page just like this and a short description about me as well.

## **Recommended Tools**

**Any Chromium-Based Browser**
* [Brave](https://brave.com/download)
* [Chromium](https://www.chromium.org/)
* [Opera](https://www.opera.com/download)
* [Microsoft Edge](https://www.microsoft.com/en-us/windows/microsoft-edge) ([soon](https://www.tomshardware.com/news/microsoft-edge-google-chrome-chromium,38192.html))

**Any Firefox-Based Browser**
* [Firefox](https://www.mozilla.org/en-US/firefox/new/)
* [Waterfox](https://www.waterfoxproject.org/)

**An IDE/Text Editor**
* [Notepad++](https://notepad-plus-plus.org/download/)
* [Visual Studio](https://visualstudio.microsoft.com/)
    * Install the [Markdown Preview](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced) extension by YiYi Wang for an improved preview from the default.
* Knowledge in how to use Git and write in Markdown.

**Git CLI/GUI**
* [Git Bash](https://git-scm.com/downloads)
* [Tortoise Git](https://tortoisegit.org/download/)

## **Recommended Resources**
* [How to create a GitHub Page](https://pages.github.com/)
* [Git CLI Tutorial](https://git-scm.com/docs/gittutorial)
* [Git Commands & Reference](https://git-scm.com/docs)
* [Markdown Guide](https://guides.github.com/features/mastering-markdown/)

## **External Sources and Reference**
* [Dr. Case's About Me Page](https://profcase.github.io/about-me-20/)
* [Average Days in a Year](https://www.rapidtables.com/calc/time/days-in-year.html)
* [isNaN Function](https://www.w3schools.com/jsref/jsref_isNaN.asp)
* [target attribute](https://www.w3schools.com/tags/att_a_target.asp)
* [Class Vue Example](https://github.com/denisecase/js-gui-vue)
* [JSON Todo List](https://jsonplaceholder.typicode.com/todos)
* [Class AJAX Example](https://github.com/profcase/js-gui-ajax)
## Contributors

**Dakota Gravitt**<br>***B.S. Computer Science***
![](images/Me%20&%20Capitol%20Building.jpg)